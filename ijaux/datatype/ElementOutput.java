package ijaux.datatype;

public interface ElementOutput  {

	public abstract boolean getOutputBoolean();

	public abstract byte getOutputByte();

	public abstract double getOutputDouble();

	public abstract float getOutputFloat();

	public abstract int getOutputInt();

	public abstract short getOutputShort();

}